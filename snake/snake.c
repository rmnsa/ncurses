#include <ncurses.h>
#include <time.h>
#include <stdlib.h>

#define MAX_X 40       /* max x of game window */
#define MAX_Y 26       /* max x of game window */
#define MAX_LENGTH 150 /* max snake length */
#define MAX_APPLES 5	  /* max visible apples */
#define CHAR_BLOCK "O" /* snake look */
#define CHAR_APPLE "*" /* apple look */
#define DEAD 'x'

typedef struct Block_ {
	int x;
	int y;
} Block;

Block snake[MAX_LENGTH];
Block apple[MAX_APPLES];
int length = 3;       /* initial length */
int score = 0;        /* initial score */
short i, speed = 130; /* frame delay in ms */
char direction = 'R';

void discardBlocks();
void addApples();
void initBlock();
void drawApples();
void checkApples();
void getDirection();
void addBlock(char);
void moveBlock();
void drawBlock();
void checkDead();
void deadAnim(int,int);
void initScreen();

WINDOW *create_newwin(int,int,int,int);
WINDOW *master;

int main() {
	initScreen();
	while(direction != DEAD) {
		timeout(speed);
		getDirection();
		drawApples();
		addBlock(direction);
		checkDead(MAX_X);
		moveBlock();
		drawBlock();
		checkApples();
		mvwprintw(master, snake[length].y, snake[length].x, " ");
		mvwprintw(stdscr ,LINES/2 - MAX_Y/2-1,COLS / 2, "Score: %d", score);
		wrefresh(master);
	}
	deadAnim(snake[0].y, snake[0].x);
	wrefresh(master);
	wgetch(master);
	endwin();
}

WINDOW *create_newwin(int y, int x, int sy, int sx) {
	WINDOW *local_win = newwin(y, x, sy, sx);
	box(local_win, 0, 0);
	wrefresh(local_win);
	return (local_win);
}

void initScreen() {
	srand(time(NULL));
	initscr();
	start_color();
	raw();
	noecho();
	curs_set(0);
	init_pair(1, COLOR_CYAN, COLOR_BLACK);
	init_pair(2, COLOR_YELLOW, COLOR_BLACK);
	init_pair(3, COLOR_GREEN, COLOR_BLACK);
	bkgd(COLOR_PAIR(2));
	master = create_newwin(MAX_Y, MAX_X, LINES/2 - MAX_Y/2, COLS/2 - MAX_X/2);
	keypad(stdscr, TRUE);
	box(master, 0, 0);
	addApples();
	initBlock();
}

void addApples() {
	for(i=0;i<MAX_APPLES;i++) {
		apple[i].x = rand() % (MAX_X - 2) + 1;
		apple[i].y = rand() % (MAX_Y - 2) + 1;
	}
}

void checkApples() {
	for(i=0;i<MAX_APPLES;i++) {
		if(snake[0].x == apple[i].x && snake[0].y == apple[i].y) {
			apple[i].x = rand() % (MAX_X - 2) + 1;
			apple[i].y = rand() % (MAX_Y - 2) + 1;
			length++;
			score++;
		}
	}
}

void drawApples() {
	for(i=0;i<MAX_APPLES;i++) {
		mvwprintw(master , apple[i].y, apple[i].x, CHAR_APPLE);
	}
}

void getDirection() {
	short c = getch();
		switch(c) {
			case KEY_LEFT:
			direction = ( direction != 'R' ) ? 'L' : 'R';
			break;;

			case KEY_RIGHT:
			direction = ( direction != 'L') ? 'R' : 'L';
			break;;

			case KEY_DOWN:
			direction = ( direction != 'U') ? 'D' : 'U';
			break;;

			case KEY_UP:
			direction = ( direction != 'D') ? 'U' : 'D';
			break;;

			case DEAD:
			direction = DEAD;
			break;;
		}
}

void addBlock(char direction) {
	snake[1] = snake [0];
	switch(direction) {
		case 'L':
		snake[0].x--;
		break;;

		case 'R':
		snake[0].x++;
		break;;

		case 'U':
		snake[0].y--;
		break;;

		case 'D':
		snake[0].y++;
		break;;
	}
}

void moveBlock() {
	for(i=length;i>0;i--) {
		snake[i+1] = snake[i];
	}
}

void checkDead() {
	if(snake[0].x >= MAX_X - 1 || snake[0].x == 0) { direction = DEAD;}
	if(snake[0].y >= MAX_Y - 1 || snake[0].y == 0) { direction = DEAD;}
	for(i=0;i<length - 1;i++) {
		if(snake[0].x == snake[i+1].x && snake[0].y == snake[i+1].y) {
			direction = DEAD; /* snake dies */
		}
	}
}

void drawBlock() {
	for(i=length;i>=0;i--) {
		wattron(master, COLOR_PAIR(1));
		mvwprintw(master, snake[i].y, snake[i].x, CHAR_BLOCK);
		wattroff(master, COLOR_PAIR(1));
	}
}

void initBlock() {
	snake[0].x = MAX_X / 2;
	snake[0].y = MAX_Y / 2;
	snake[1].x = snake[0].x++;
	snake[1].y = snake[0].y;
	snake[2].x = snake[1].x++;
	snake[2].y = snake[1].y;
}

void deadAnim(int y, int x) {
	mvwprintw(master, y, x, "X");
	mvwprintw(master, y-1, x+1, "X");
	mvwprintw(master, y-1, x-1, "X");
	mvwprintw(master, y+1, x+1, "X");
	mvwprintw(master, y+1, x-1, "X");
	mvwprintw(master, y+1, x, " ");
	mvwprintw(master, y-1, x, " ");
	mvwprintw(master, y, x+1, " ");
	mvwprintw(master, y, x-1, " ");
}
