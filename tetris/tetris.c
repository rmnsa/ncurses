#include <ncurses.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>
#define WINX 26
#define WINY 30
#define MARGIN 3

typedef struct Part_ {
	short x, y;
} Part;

Part Lshape[5] = {{0,0}, {0,1}, {0,2}, {1,2}, {0,3}};

WINDOW *master, *scoreboard, *shapetable;
short t, c;

WINDOW *createwin(short y, short x, short sy, short sx) {
	WINDOW *local = newwin(y, x, sy, sx);
	box(local, 0, 0); wrefresh(local);
	return(local);
}

void *timer() { /* timer in separate thread */
	for(t=0;t<999;t++) { /* limit max time to 999 */
	mvwprintw(scoreboard, 1, 2, "Time:%d", t);
	wrefresh(scoreboard);
	sleep(1);
	}
}

int main() {
	initscr();
	noecho();
	curs_set(0);

	master = createwin(WINY, WINX, LINES/2 - WINY/2, COLS/2 - WINX/2);
	scoreboard = createwin(WINY, WINX/2, LINES/2 - WINY/2, COLS/2 - WINX);
	shapetable = createwin(WINY, WINX/2, LINES/2 - WINY/2, COLS/2 + WINX/2);

	keypad(master, TRUE);
	pthread_t timert; pthread_create(&timert, NULL, timer, NULL);

	while((c = wgetch(master)) != 'x') {
	wclear(master);
	box(master, 0, 0);
	wrefresh(master);
	}
	endwin();
}
