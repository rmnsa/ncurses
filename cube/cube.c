#include <ncurses.h>
#include <math.h>
#include <string.h>
#include <unistd.h>

#define PI 3.14159
#define U 40           /* cube side length */
#define BLOCK_CHAR "+" /* building char for cube */
#define MAX_X 120      /* x-axis length of projection window */
#define MAX_Y 60       /* y-axis height of projection window */

typedef struct Point3d_ {
	int x, y, z;
} Point3d;

WINDOW *canvas;

WINDOW *createwin(short y, short x, short sy, short sx) {
	WINDOW *local = newwin(y, x, sy, sx);
	box(local, 0, 0); wrefresh(local);
	return(local);
}

	/* line drawing algorithm */
void drawLine(const int x1,const short y1,const short x2, const short y2) {
	int d, dx, dy, ai, bi, xi, yi;
	short x = x1, y = y1;
	if (x1 < x2) { /* get direction x */
		xi = 1;
		dx = x2 - x1;
	} else {
		xi = -1;
		dx = x1 - x2;
	}
	if(y1 < y2) { /* get direction y */
		yi = 1;
		dy = y2 - y1;
	} else {
		yi = -1;
		dy = y1 - y2;
	}
	mvwprintw(canvas, y, x, BLOCK_CHAR); /* first pixel */

	if(dx > dy) { /* leading OX-axis */
		ai = (dy - dx) * 2;
		bi = dy * 2;
		d = bi - dx;
		while(x != x2) {
			if(d >= 0) {
				x += xi;
				y += yi;
				d += ai;
			} else {
				d += bi;
				x += xi;
			}
			mvwprintw(canvas, y, x, BLOCK_CHAR);
		}
	} else { /* leading OY-axis */
		ai = (dx - dy) * 2;
		bi = dx * 2;
		d = bi - dy;
		while(y != y2) {
			if(d >= 0) {
				x += xi;
				y += yi;
				d += ai;
			} else {
				d += bi;
				y += yi;
			}
			mvwprintw(canvas, y, x, BLOCK_CHAR);
		}
	}
}
	/* matrix multiplication scheme */
void multMatrix4(Point3d *in, Point3d *out, float mat[][4]) {
	out->x = in->x * mat[0][0] + in->y * mat[1][0] + in->z * mat[2][0] + mat[3][0];
	out->y = in->x * mat[0][1] + in->y * mat[1][1] + in->z * mat[2][1] + mat[3][1];
	out->z = in->x * mat[0][2] + in->y * mat[1][2] + in->z * mat[2][2] + mat[3][2];
	float n = in->x * mat[0][3]+in->y * mat[1][3] + in->z * mat[2][3] + mat[3][3];
	if(n != 0) { out->x /= n; out->y /= n; out->z /= n; }
}

int main() {
	initscr();
	noecho();
	mvprintw(10, 10, "PRESS X TO EXIT");
	canvas = createwin(MAX_Y, MAX_X, LINES/2 - MAX_Y/2, COLS/2 - MAX_X/2);
	curs_set(0); /* hide cursor */
	float i = 1; int j, c;
	float near = 35; float far = 1000.0; float fov = 30.0;
	float ratio = COLS / LINES; float radius = 25.0 / tan(fov * 0.5 / 180 * PI);
	while(c != 'x') {
		float proj[4][4] = { /* 3d projection matrix */
			{ratio * radius, 0, 0, 0},
			{0, radius, 0,                          0},
			{0, 0,      far/(far - near),           1},
			{0, 0,      (-far * near)/(far - near), 0}
		};
		float rotX[4][4] = { /* x-axis rotation matrix */
			{1, 0,       0,      0},
			{0, cos(i),  sin(i), 0},
			{0, -sin(i), cos(i), 0},
			{0, 0,       0,      1}
		};
		float rotZ[4][4] = { /* z-axis rotation matrix */
			{cos(i*0.5),  sin(i*0.5), 0, 0},
			{-sin(i*0.5), cos(i*0.5), 0, 0},
			{0,           0,          1, 0},
			{0,           0,          0, 1}
		};
		
		Point3d p[36], pp[36], px[36], pz[36]; /* define cube coordinates */
		/* front */
		p[0].x = 0; p[0].y = 0;p[0].z = 0;
		p[1].x = 0; p[1].y = U;p[1].z = 0;
		p[2].x = U; p[2].y = U;p[2].z = 0;
		p[3].x = U; p[3].y = U;p[3].z = 0;
		p[4].x = U; p[4].y = 0;p[4].z = 0;
		p[5].x = 0; p[5].y = 0;p[5].z = 0;
		/* back */
		p[6].x = 0; p[6].y = 0;p[6].z = U;
		p[7].x = 0; p[7].y = U;p[7].z = U;
		p[8].x = U; p[8].y = U;p[8].z = U;
		p[9].x = U; p[9].y = U;p[9].z = U;
		p[10].x = U; p[10].y = 0;p[10].z = U;
		p[11].x = 0; p[11].y = 0;p[11].z = U;
		/* right */
		p[12].x = U; p[12].y = U;p[12].z = 0;
		p[13].x = U; p[13].y = 0;p[13].z = 0;
		p[14].x = U; p[14].y = U;p[14].z = U;
		p[15].x = U; p[15].y = U;p[15].z = U;
		p[16].x = U; p[16].y = 0;p[16].z = U;
		p[17].x = U; p[17].y = 0;p[17].z = 0;
		/* left */
		p[18].x = 0; p[18].y = U;p[18].z = 0;
		p[19].x = 0; p[19].y = 0;p[19].z = 0;
		p[20].x = 0; p[20].y = U;p[20].z = U;
		p[21].x = 0; p[21].y = U;p[21].z = 0;
		p[22].x = 0; p[22].y = U;p[22].z = U;
		p[23].x = U; p[23].y = U;p[23].z = U;
		/* up */
		p[24].x = U; p[24].y = U;p[24].z = U;
		p[25].x = U; p[25].y = U;p[25].z = 0;
		p[26].x = 0; p[26].y = U;p[26].z = 0;
		p[27].x = 0; p[27].y = 0;p[27].z = 0;
		p[28].x = 0; p[28].y = 0;p[28].z = U;
		p[29].x = U; p[29].y = 0;p[29].z = U;
		/* down */
		p[30].x = U; p[30].y = 0;p[30].z = U;
		p[31].x = U; p[31].y = 0;p[31].z = 0;
		p[32].x = 0; p[32].y = 0;p[32].z = 0;
		p[33].x = 0; p[33].y = U;p[33].z = 0;
		p[34].x = 0; p[34].y = U;p[34].z = U;
		p[35].x = U; p[35].y = U;p[35].z = U;
	
		for(j=0;j<sizeof(p)/sizeof(p[0]);j++) { /* projection */
			multMatrix4(&p[j], &px[j], rotX);
			multMatrix4(&px[j], &pz[j], rotZ);
			pz[j].z += 300; /* offset cube to the back */
			multMatrix4(&pz[j], &pp[j], proj);
		}
		for(j=0;j<sizeof(p)/sizeof(p[0]);j++) { /* print corners of the cube */
			mvwprintw(canvas, pp[j].y + MAX_Y * 0.33, pp[j].x + MAX_X * 0.5, BLOCK_CHAR);
		}
		for(j=0;j<sizeof(p)/sizeof(p[0]);j+=3) { /* print lines from corners */
		drawLine(pp[0+j].x + MAX_X * 0.5, pp[0+j].y + MAX_Y * 0.66,
					pp[1+j].x + MAX_X * 0.5, pp[1+j].y + MAX_Y * 0.66);
		drawLine(pp[1+j].x + MAX_X * 0.5, pp[1+j].y + MAX_Y * 0.66,
					pp[2+j].x + MAX_X * 0.5, pp[2+j].y + MAX_Y * 0.66);
		drawLine(pp[2+j].x + MAX_X * 0.5, pp[2+j].y + MAX_Y * 0.66,
					pp[0+j].x + MAX_X * 0.5, pp[0+j].y + MAX_Y * 0.66);
		}
		i += 0.03; /* rotation offset per frame */
		wrefresh(canvas);
		timeout(50);
		c = getch();
		wclear(canvas);
	}
	getch();
	endwin();
}
